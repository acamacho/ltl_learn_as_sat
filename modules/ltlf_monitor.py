from ext.ltlfkit import ltlf2hoa
from ext.automata import get_automaton
from modules.ltlf_realizability import get_qstate_transitions, neg_fml
import spot


def state_models_guard(state,guard):
    # assumes a closed-world representation of the state
    for literal in guard:
        if literal.startswith("!"):
            predicate = literal[1:] # remove negation
            if predicate in state:
                return False
        else:
            predicate = literal
            if predicate not in state:
                return False
    return True

def monitor(formula,trace):
    # return a finite trace that satisfies the input formula
    automaton = get_automaton(formula, "nfw", "ltl2sa", None)
    
    # detect UNSAT
    if automaton.get_init_state_number() == 0 and automaton.num_states() == 1:
        return None
    
    Q_accepting_dest = automaton.get_accepting_dest()
    
    # progress the run of the automaton on the given trace
    i = 0
    cur_qstate = automaton.get_init_state_number()
    while i < len(trace):
        can_progress = False
        transitions = get_qstate_transitions(automaton,cur_qstate)
        for transition in transitions:
            guard = transition.guard
            
            # check if trace[i] models the guard
            can_progress = state_models_guard(trace[i],guard)
            
            if can_progress:
                cur_qstate = transition.dst_id
                i += 1
                break
        
        if not can_progress:
            # progression failed
            return False
    
    # all the trace was progressed
    # check whether the last automaton state is accepting
    return (cur_qstate in Q_accepting_dest)
    