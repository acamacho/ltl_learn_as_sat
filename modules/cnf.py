#!/usr/bin/python3
import enum 
import pycosat  
# from pysat.solvers import Glucose3,Lingeling,Minisat22,Minicard

AUGMENT_OPERATORS = True
# AUGMENT_OPERATORS = False

ABSTRACT_EXAMPLES = False

def neg_predicate(predicate):
    return "!%s" % predicate

# creating enumerations using class 
class Skeletons(enum.Enum):
    literal = 1
    lor = 2
    land = 3
    next = 4
    until = 5
    release = 6
    weaknext = 7
    if AUGMENT_OPERATORS:
        eventually = 8
        always = 9

class VarTypes(enum.Enum):
    ETS = 1 # var(example_id, timestep, skeleton_id)
    ST = 2 # varType(skeleton_id, skeleton_type)
    alpha = 3 # varAlpha(skeleton_id, skeleton_id')
    beta = 4 # varBeta(skeleton_id, skeleton_id')
    skliteral = 5 # var(skeleton_id, literal)
    if ABSTRACT_EXAMPLES:
        expredicate = 6 # var(example,timestep,predicate)

# class that represents a SAT variable
class Variable():
    def __init__(self, vartype):
        self._id = None
        self.vartype = vartype
        
        self.example_id = None
        self.timestep = None
        self.skeleton_id = None
        
        self.skeleton_type = None
        
        self.alpha = None
        self.beta = None
        
        self.literal = None
    
class VarsDict():
    def __init__(self):
        self.lookupTable = {}

        self.examples = {}
        self.skeletons = {}
        self.alphas = {}
        self.betas = {}
        self.exliterals = {}
        self.skliterals = {}
        if ABSTRACT_EXAMPLES:
            self.expredicates = {}
        
    def add_variable(self,variable):
        variable_id = len(self.lookupTable.keys()) + 1
        variable._id = variable_id
        self.lookupTable[variable._id] = variable
        
        if variable.vartype == VarTypes.ETS:
            example_id = variable.example_id
            if example_id not in self.examples:
                self.examples[example_id] = {}
            timestep = variable.timestep
            if timestep not in self.examples[example_id]:
                self.examples[example_id][timestep] = {}
            skeleton_id = variable.skeleton_id
            self.examples[example_id][timestep][skeleton_id] = variable
            # print("VAR_ID %s: variable VarETS(%s,%s,%s)" % (variable._id,example_id,timestep,skeleton_id))
            
        elif variable.vartype == VarTypes.ST:
            skeleton_id = variable.skeleton_id
            if skeleton_id not in self.skeletons:
                self.skeletons[skeleton_id] = {}
            skeleton_type = variable.skeleton_type
            self.skeletons[skeleton_id][skeleton_type] = variable
            # print("VAR_ID %s: variable VarST(%s,%s)" % (variable._id,skeleton_id,skeleton_type))
        
        # elif variable.vartype == VarTypes.ETL:
        #     example_id = variable.example_id
        #     if example_id not in self.examples:
        #         self.examples[example_id] = {}
        #     timestep = variable.timestep
        #     if timestep not in self.examples[example_id]:
        #         self.examples[example_id][timestep] = {}
        #     literal = variable.literal
        #     self.examples[example_id][timestep][literal] = variable
            
        elif variable.vartype == VarTypes.alpha:
            skeleton_id = variable.skeleton_id
            if skeleton_id not in self.alphas:
                self.alphas[skeleton_id] = {}
            alpha_skeleton_id = variable.alpha
            assert(alpha_skeleton_id not in self.alphas[skeleton_id])
            self.alphas[skeleton_id][alpha_skeleton_id] = variable
            # print("VAR_ID %s: variable VarAlpha(%s,%s)" % (variable._id,skeleton_id,alpha_skeleton_id))
        
        elif variable.vartype == VarTypes.beta:
            skeleton_id = variable.skeleton_id
            if skeleton_id not in self.betas:
                self.betas[skeleton_id] = {}
            beta_skeleton_id = variable.beta
            assert(beta_skeleton_id not in self.betas[skeleton_id])
            self.betas[skeleton_id][beta_skeleton_id] = variable
            # print("VAR_ID %s: variable VarBeta(%s,%s)" % (variable._id,skeleton_id,beta_skeleton_id))
            
        elif variable.vartype == VarTypes.skliteral:
            # var(s,p)
            skeleton_id = variable.skeleton_id
            if skeleton_id not in self.skliterals:
                self.skliterals[skeleton_id] = {}
            literal = variable.literal
            assert(literal not in self.skliterals[skeleton_id])
            self.skliterals[skeleton_id][literal] = variable
            # print("VAR_ID %s: variable VarLit(%s,%s)" % (variable._id,skeleton_id,literal))
        else:
            assert(ABSTRACT_EXAMPLES)
            assert(variable.vartype == VarTypes.expredicate)
            # var(e,t,p)
            example_id = variable.example_id
            if example_id not in self.expredicates:
                self.expredicates[example_id] = {}
            timestep = variable.timestep
            if timestep not in self.expredicates[example_id]:
                self.expredicates[example_id][timestep] = {}
            predicate = variable.predicate
            assert(predicate not in self.expredicates[example_id][timestep])
            self.expredicates[example_id][timestep][predicate] = variable
            # print("VAR_ID %s: variable VarEx(%s,%s,%s)" % (variable._id,example_id,timestep,predicate))

    
    def varETS_id(self,example_id,timestep,skeleton_id):
        return self.examples[example_id][timestep][skeleton_id]._id

    def varST_id(self,skeleton_id,skeleton_type):
        return self.skeletons[skeleton_id][skeleton_type]._id

    def varETL_id(self,example_id,timestep,literal):
        return self.examples[example_id][timestep][literal]._id

    def varAlpha_id(self,skeleton_id,alpha_skeleton_id):
        return self.alphas[skeleton_id][alpha_skeleton_id]._id

    def varBeta_id(self,skeleton_id,beta_skeleton_id):
        return self.betas[skeleton_id][beta_skeleton_id]._id

    def varLit_id(self,skeleton_id,literal):
        return self.skliterals[skeleton_id][literal]._id
    
    if ABSTRACT_EXAMPLES:
        def varEx_id(self,example_id,timestep,predicate):
            return self.expredicates[example_id][timestep][predicate]._id

class CNF():
    def __init__(self,fml_size,vocabulary):
        self.fml_size = fml_size
        self.vocabulary = vocabulary
        self.varsDict = VarsDict()
        self.num_examples = 0
        
        self.clauses = []

        self.gen_initial_variables()
        self.clauses.extend(self.gen_initial_clauses())
        
        self.true = "((%s) | !(%s))" % (self.vocabulary[0],self.vocabulary[0])
        
    def recover_fml_from_model(self,model):
        def get_skeleton_type(self, model, skeleton_id):
            for skeleton_type in Skeletons:
                varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
                if varST_id in model:
                    return skeleton_type
            raise("Error: no skeleton type in model")
            
        def get_alpha_id(self,skeleton_id):
            for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                if varAlpha_id in model:
                    return alpha_skeleton_id
            raise("Error: Alpha skeleton not in model")

        def get_beta_id(self,skeleton_id):
            for beta_skeleton_id in range(skeleton_id+2,self.fml_size):
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
                if varBeta_id in model:
                    return beta_skeleton_id
            raise("Error: Beta skeleton not in model")
            
        def get_literal(self,skeleton_id):
            for p in range(len(self.vocabulary)):
                literal = p+1
                varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
                if varLit_id in model:
                    return self.vocabulary[p]
                literal = -(p+1)
                varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
                if varLit_id in model:
                    return neg_predicate(self.vocabulary[p])
            raise("Error: Literal skeleton not in model")
            
        def skeleton_to_formula(self, model, cur_skeleton_id):
            cur_skeleton_type = get_skeleton_type(self, model, cur_skeleton_id)
            if cur_skeleton_type == Skeletons.land:
                lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                rhs_varBeta_id = get_beta_id(self,cur_skeleton_id)
                lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                rhs_fml = skeleton_to_formula(self, model, rhs_varBeta_id)
                return "( (%s) & (%s) )" % (lhs_fml,rhs_fml)
            elif cur_skeleton_type == Skeletons.lor:
                lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                rhs_varBeta_id = get_beta_id(self,cur_skeleton_id)
                lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                rhs_fml = skeleton_to_formula(self, model, rhs_varBeta_id)
                return "( (%s) | (%s) )" % (lhs_fml,rhs_fml)
            elif cur_skeleton_type == Skeletons.next:
                lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                return "( X (%s) )" % (lhs_fml)
            elif cur_skeleton_type == Skeletons.until:
                lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                rhs_varBeta_id = get_beta_id(self,cur_skeleton_id)
                lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                rhs_fml = skeleton_to_formula(self, model, rhs_varBeta_id)
                return "( (%s) U (%s) )" % (lhs_fml,rhs_fml)
            elif cur_skeleton_type == Skeletons.weaknext:
                lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                return "( (! X (%s)) | X (%s) )" % (self.true,lhs_fml)
            elif cur_skeleton_type == Skeletons.release:
                lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                rhs_varBeta_id = get_beta_id(self,cur_skeleton_id)
                lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                rhs_fml = skeleton_to_formula(self, model, rhs_varBeta_id)
                return "( (%s) R (%s) )" % (lhs_fml,rhs_fml)
            elif cur_skeleton_type == Skeletons.literal:
                literal = get_literal(self,cur_skeleton_id)
                return "(%s)" % (literal)
            elif AUGMENT_OPERATORS:
                if cur_skeleton_type == Skeletons.eventually:
                    lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                    lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                    return "( F (%s) )" % (lhs_fml)
                elif cur_skeleton_type == Skeletons.always:
                    lhs_varAlpha_id = get_alpha_id(self,cur_skeleton_id)
                    lhs_fml = skeleton_to_formula(self, model, lhs_varAlpha_id)
                    return "( G (%s) )" % (lhs_fml)
        
        # transform model to string formula
        cur_skeleton_id = 0
        formula = skeleton_to_formula(self,model, cur_skeleton_id)
        return formula
    
    def get_model(self):
        # g = Glucose3()
        # g = Minicard()
        # for clause in self.clauses: g.add_clause(clause)
        # if g.solve(): sol = g.get_model()
        # else: sol = "UNSAT"
        sol = pycosat.solve(self.clauses)
        if sol == "UNSAT":
            print("... no model exists.")
            return None
        formula = self.recover_fml_from_model(sol)
        return formula
    
    def add_pos_example(self, example):
        # example = [["a","b"], ["b","c"] ]
        self.gen_variables_for_example(example)
        self.clauses.extend(self.gen_skeleton_clauses_for_pos_example(example))
        self.num_examples += 1

    def add_neg_example(self, example):
        self.gen_variables_for_example(example)
        self.clauses.extend(self.gen_skeleton_clauses_for_neg_example(example))
        self.num_examples += 1
    
    def gen_initial_variables(self):
        # these formulae are var(s,type) where 
        # s is a skeleton_id and
        # type is a skeleton type
        for skeleton_id in range(self.fml_size):
            for skeleton_type in Skeletons:
                new_variable = Variable(VarTypes.ST)
                new_variable.skeleton_id = skeleton_id
                new_variable.skeleton_type = skeleton_type
                # create new entry in the dictionary
                self.varsDict.add_variable(new_variable)

        # these formulae are varAlpha(s,s') and varBeta(s,s'),
        # where s,s' are skeleton_ids
        # in order to avoid loops, we only consider 
        # skeletonAlpha_id > skeleton_id
        # skeletonBeta_id > skeletonAlpha_id        
        for skeleton_id in range(self.fml_size):
            for skeleton_alpha_id in range(skeleton_id+1, self.fml_size):
                new_variable = Variable(VarTypes.alpha)
                new_variable.skeleton_id = skeleton_id
                new_variable.alpha = skeleton_alpha_id
                # create new entry in the dictionary
                self.varsDict.add_variable(new_variable)

            for skeleton_beta_id in range(skeleton_id+2, self.fml_size):
                new_variable = Variable(VarTypes.beta)
                new_variable.skeleton_id = skeleton_id
                new_variable.beta = skeleton_beta_id     
                # create new entry in the dictionary
                self.varsDict.add_variable(new_variable)

        # these formulae are varLit(s,p)
        # where s is a skeleton_id, and
        # p is a literal
        for skeleton_id in range(self.fml_size):
            for p in range(len(self.vocabulary)):
                # new_variable_id = len(self.dictionary.keys())
                new_variable = Variable(VarTypes.skliteral)
                new_variable.skeleton_id = skeleton_id
                new_variable.literal = p+1
                # create new entry in the dictionary
                self.varsDict.add_variable(new_variable)
                # self.dictionary[new_variable_id] = new_variable

                # new_variable_id = len(self.dictionary.keys())
                new_variable = Variable(VarTypes.skliteral)
                new_variable.skeleton_id = skeleton_id
                new_variable.literal = -(p+1)
                # create new entry in the dictionary
                self.varsDict.add_variable(new_variable)
                # self.dictionary[new_variable_id] = new_variable

    def gen_initial_clauses(self):

        def at_most_one(ids):
            clauses = []
            # pairwise mutexes:
            for i in range(len(ids)):
                for j in range(i+1,len(ids)):
                    clause = [-ids[i],-ids[j]]
                    clauses.append(clause)
            return clauses
            
        def exactly_one(ids):
            clauses = []
            # pairwise mutexes:
            clauses.extend(at_most_one(ids))
            # at least one:
            clauses.append(ids)
            return clauses
            
        clauses = []
        
        # oneof(s,type)
        for skeleton_id in range(self.fml_size):
            ST_ids = []
            for skeleton_type in Skeletons:
                variable_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
                ST_ids.append(variable_id)
                
            clauses.extend(exactly_one(ST_ids))
        
        # oneof varAlpha(s,s')
        for skeleton_id in range(self.fml_size):
            Alpha_ids = []
            for skeleton_alpha_id in range(skeleton_id+1,self.fml_size):
                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,skeleton_alpha_id)
                Alpha_ids.append(varAlpha_id)
            if(len(Alpha_ids) > 0):
                clauses.extend(exactly_one(Alpha_ids))
        
        # oneof varBeta(s,s')
        for skeleton_id in range(self.fml_size):
            Beta_ids = []
            for skeleton_beta_id in range(skeleton_id+2,self.fml_size):
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,skeleton_beta_id)
                Beta_ids.append(varBeta_id)
            if(len(Beta_ids) > 0):
                clauses.extend(exactly_one(Beta_ids))
        
        # at most one varLit(s,p)
        for skeleton_id in range(self.fml_size):
            Lit_ids = []
            for p in range(len(self.vocabulary)):
                literal = p+1
                varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
                Lit_ids.append(varLit_id)
                literal = -(p+1)
                varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
                Lit_ids.append(varLit_id)
            clauses.extend(at_most_one(Lit_ids))
            
            # if s is lit, then at least one varLit(s,p)
            skeleton_type = Skeletons.literal
            varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
            clause = [-varST_id] + Lit_ids
            clauses.append(clause)
            
            # clauses.extend(at_most_one(Lit_ids))
            # # if s is lit, then at least one varLit(s,p)
            # for skeleton_id in range(self.fml_size):
            #     skeleton_type = Skeletons.literal
            #     varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
            #     clause = [-varST_id] + Lit_ids
            #     clauses.append(clause)
    
    
        # force not to have until, releases, and, or, with skeleton_id >= fml_len - 2
        for skeleton_id in range(self.fml_size-2,self.fml_size):
            for skeleton_type in [Skeletons.until, Skeletons.release, Skeletons.land, Skeletons.lor]:
                varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
                clause = [-varST_id]
                clauses.append(clause)
        
        # force not to have next and weaknext operators with skeleton_id >= fml_len - 1
        for skeleton_id in range(self.fml_size-1,self.fml_size):
            unary_operators = [Skeletons.next, Skeletons.weaknext]
            if AUGMENT_OPERATORS:
                unary_operators += [Skeletons.eventually, Skeletons.always]
            for skeleton_type in unary_operators:
                varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
                clause = [-varST_id]
                clauses.append(clause)

        return clauses

    # generates a list of clauses (CNF) for a positive example
    def gen_variables_for_example(self,example):
        # these formulae are var(e,t,s) where e is an example_id, 
        # t is a timestep, and
        # s is a subformula_id
        example_id = self.num_examples
        # self.num_examples += 1
        for timestep in range(len(example)):
            for skeleton_id in range(self.fml_size):
                new_variable = Variable(VarTypes.ETS)
                new_variable.example_id = example_id
                new_variable.timestep = timestep
                new_variable.skeleton_id = skeleton_id
                
                # create new entry in the dictionary
                self.varsDict.add_variable(new_variable)
        
        if ABSTRACT_EXAMPLES:
            # these formulae are var(e,t,p) where e is an example_id, 
            # t is a timestep, and
            # s is a predicate
            for timestep in range(len(example)):
                for p in range(len(self.vocabulary)):
                    new_variable = Variable(VarTypes.expredicate)
                    new_variable.example_id = example_id
                    new_variable.timestep = timestep
                    new_variable.predicate = p+1
                    
                    # create new entry in the dictionary
                    self.varsDict.add_variable(new_variable)
        
        
    def gen_skeleton_clauses_for_pos_example(self,example):
        # these clauses are var(e,t,s) where e is an example_id, 
        # t is a timestep, and
        # s is a skeleton_id
        clauses = []
        example_id = self.num_examples
        
        first_timestep = 0
        first_skeleton_id = 0
        varETS_id = self.varsDict.varETS_id(example_id,first_timestep,first_skeleton_id)
        clauses.extend( [[varETS_id]] )
        # for skeleton_id in range(1,self.fml_size):
        #     varETS_id = self.varsDict.varETS_id(example_id,first_timestep,skeleton_id)
        #     clauses.extend( [[-varETS_id]] )
        
        # self.num_examples += 1
        for timestep in range(len(example)):
            for skeleton_id in range(self.fml_size):
                # and
                clauses.extend( self.get_clauses_and(example_id,timestep,skeleton_id) )
                # or
                clauses.extend( self.get_clauses_or(example_id,timestep,skeleton_id) )
                # next
                clauses.extend( self.get_clauses_next(example_id,timestep,skeleton_id,len(example)) )
                # until
                clauses.extend( self.get_clauses_until(example_id,timestep,skeleton_id,len(example)) )
                # release
                clauses.extend( self.get_clauses_release(example_id,timestep,skeleton_id,len(example)) )
                # weaknext
                clauses.extend( self.get_clauses_weaknext(example_id,timestep,skeleton_id,len(example)) )
                if AUGMENT_OPERATORS:
                    # eventually
                    clauses.extend( self.get_clauses_eventually(example_id,timestep,skeleton_id,len(example)) )
                    # always
                    clauses.extend( self.get_clauses_always(example_id,timestep,skeleton_id,len(example)) )
                # # literals
                # clauses.extend( self.get_clauses_exliterals(example_id,timestep,observation)
                observation = example[timestep]
                clauses.extend(self.get_clauses_literal(example_id,timestep,skeleton_id,observation) )
        
        # at the end of the example, the non-terminal skeletons need to be false
        # this is forced by the construction of the skeleton clauses above
        
        # print(clauses)
        return clauses
        
        
    def gen_skeleton_clauses_for_neg_example(self,example):
        # these clauses are var(e,t,s) where e is an example_id, 
        # t is a timestep, and
        # s is a skeleton_id
        clauses = []
        example_id = self.num_examples
        
        first_timestep = 0
        first_skeleton_id = 0
        # print("accessing ex=%s time=%s sk=%s" % (example_id,first_timestep,first_skeleton_id))
        varETS_id = self.varsDict.varETS_id(example_id,first_timestep,first_skeleton_id)
        clauses.extend( [[varETS_id]] )
        # for skeleton_id in range(1,self.fml_size):
        #     varETS_id = self.varsDict.varETS_id(example_id,first_timestep,skeleton_id)
        #     clauses.extend( [[-varETS_id]] )
            
        # self.num_examples += 1
        for timestep in range(len(example)):
            for skeleton_id in range(self.fml_size):
                # and
                clauses.extend( self.get_dual_clauses_and(example_id,timestep,skeleton_id) )
                # or
                clauses.extend( self.get_dual_clauses_or(example_id,timestep,skeleton_id) )
                # next
                clauses.extend( self.get_dual_clauses_next(example_id,timestep,skeleton_id,len(example)) )
                # until
                clauses.extend( self.get_dual_clauses_until(example_id,timestep,skeleton_id,len(example)) )
                # release
                clauses.extend( self.get_dual_clauses_release(example_id,timestep,skeleton_id,len(example)) )
                # weaknext
                clauses.extend( self.get_dual_clauses_weaknext(example_id,timestep,skeleton_id,len(example)) )
                if AUGMENT_OPERATORS:
                    # eventually
                    clauses.extend( self.get_dual_clauses_eventually(example_id,timestep,skeleton_id,len(example)) )
                    # always
                    clauses.extend( self.get_dual_clauses_always(example_id,timestep,skeleton_id,len(example)) )
                # # literals
                # clauses.extend( self.get_clauses_exliterals(example_id,timestep,observation)
                observation = example[timestep]
                clauses.extend(self.get_dual_clauses_literal(example_id,timestep,skeleton_id,observation) )
        
        # at the end of the example, the non-terminal skeletons need to be false
        # this is forced by the construction of the skeleton clauses above
        
        # print(clauses)
        return clauses

    # assuming concrete examples
    def get_clauses_literal(self,example_id,timestep,skeleton_id,observation):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.literal
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

        for p in range(len(self.vocabulary)):
            literal = p+1
            varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
            if self.vocabulary[p] not in observation:
                # this cannot happen
                clause = [-varETS_id, -varST_id, -varLit_id]
                clauses.append(clause)
            
            literal = -(p+1)
            varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
            if self.vocabulary[p] in observation:
                # this cannot happen
                clause = [-varETS_id, -varST_id, -varLit_id]
                clauses.append(clause)
        
        return clauses
        
    # def get_clauses_literal(self,example_id,timestep,skeleton_id,observation):
    #     clauses = []
    #     varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
    #     skeleton_type = Skeletons.literal
    #     varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

    #     for p in range(len(self.vocabulary)):
    #         # literal has to hold true in the example
    #         literal = p+1
    #         varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
    #         # if self.vocabulary[p] in observation:
    #         #     # this clause is trivially true
    #         #     # clause = [-varETS_id, -varST_id, -varLit_id, "True"]
    #         #     # clauses.append(clause)
    #         #     continue
    #         # elif neg_predicate(self.vocabulary[p]) in observation:
    #         if neg_predicate(self.vocabulary[p]) in observation:
    #             # clause = [-varETS_id, -varST_id, -varLit_id, "False"]
    #             clause = [-varETS_id, -varST_id, -varLit_id]
    #             clauses.append(clause)

    #         # literal has to hold false in the example
    #         literal = -(p+1)
    #         varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
    #         # if neg_predicate(self.vocabulary[p]) in observation:
    #         #     # this clause is trivially true
    #         #     # clause = [-varETS_id, -varST_id, -varLit_id, "True"]
    #         #     # clauses.append(clause)
    #         #     continue
    #         if self.vocabulary[p] in observation:
    #             # clause = [-varETS_id, -varST_id, -varLit_id, "False"]
    #             clause = [-varETS_id, -varST_id, -varLit_id]
    #             clauses.append(clause)            
        
    #     if ABSTRACT_EXAMPLES:
    #         # Necessary when using abstractions in the input examples
    #         # in order to avoid pathological issues with formulas X(p | !p) and
    #         # negative examples [[a],[!a]] for target a & Xa
    #         # IDEA: set X variable in observation!
    #         # if var(e,t,s) var(s,lit) varLit(s',p) then varEx(e,t,p) and !varEx(e,t,!p)
    #         for p in range(len(self.vocabulary)):
    #             pred = p+1
    #             varPred_id = self.varsDict.varLit_id(skeleton_id,pred)
    #             varEx_id = self.varsDict.varEx_id(example_id,timestep,pred)
    #             clause = [-varETS_id, -varST_id, -varPred_id,varEx_id]
    #             clauses.append(clause)
    
    #             neg_pred = -(p+1)
    #             varNegPred_id = self.varsDict.varLit_id(skeleton_id,neg_pred)
    #             clause = [-varETS_id, -varST_id, -varNegPred_id,-varEx_id]
    #             clauses.append(clause)
        
    #     return clauses
        
        # for p in range(1,1+len(self.vocabulary)):
        #     var_id = self.varsDict.varETL_id(example_id,timestep,p)
        #     negvar_id = self.varsDict.varETL_id(example_id,timestep,-p)
            
        #     if self.vocabulary[p-1] in observation:
        #         assert (neg_predicate(self.vocabulary[p-1]) not in observation )
        #         clause = [ var_id ]
        #         clauses.append( clause )
        
        #         clause = [ -negvar_id ]
        #         clauses.append( clause )
                
        #     elif neg_predicate(self.vocabulary[p-1]) in observation:
        #         assert(self.vocabulary[p-1] not in observation)
        #         clause = [ -var_id ]
        #         clauses.append( clause )
        
        #         clause = [ negvar_id ]
        #         clauses.append( clause )
        
        # # print(clauses)
        # return clauses
        
    # def get_clauses_exliterals(self,example_id,timestep,observation):
    #     clauses = []
    #     for p in range(1,1+len(self.vocabulary)):
    #         var_id = self.varsDict.varETL_id(example_id,timestep,p)
    #         negvar_id = self.varsDict.varETL_id(example_id,timestep,-p)
            
    #         if self.vocabulary[p-1] in observation:
    #             assert (neg_predicate(self.vocabulary[p-1]) not in observation )
    #             clause = [ var_id ]
    #             clauses.append( clause )
        
    #             clause = [ -negvar_id ]
    #             clauses.append( clause )
                
    #         elif neg_predicate(self.vocabulary[p-1]) in observation:
    #             assert(self.vocabulary[p-1] not in observation)
    #             clause = [ -var_id ]
    #             clauses.append( clause )
        
    #             clause = [ negvar_id ]
    #             clauses.append( clause )
        
    #     # print(clauses)
    #     return clauses

    def get_clauses_and(self,example_id,timestep,skeleton_id):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.land
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
        #     for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                clause_alpha = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
                clauses.append(clause_alpha)
                
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                clauses.append(clause_beta)
            
        return clauses
    
    def get_clauses_or(self,example_id,timestep,skeleton_id):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.lor
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
        #     for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, -varBeta_id, varETS_alpha_id, varETS_beta_id]
                clauses.append(clause)

        return clauses
    
    def get_clauses_next(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.next
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            
            varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)

            if timestep == len_example-1:
                clause = [-varETS_id, -varST_id, -varAlpha_id]
                clauses.append(clause)
            else:
                varETS_alpha_prime_id = self.varsDict.varETS_id(example_id,timestep+1,alpha_skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_prime_id]
                clauses.append(clause)

        return clauses
    
    def get_clauses_until(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.until
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
            # for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                
                if timestep+1 < len_example:
                    # either ((aUb)' and a) or b \equiv ((aUb)' or b) and (a or b)
                    clause_alpha_or_beta = [-varETS_id, -varST_id, -varAlpha_id, -varBeta_id, varETS_alpha_id, varETS_beta_id]
                    clauses.append(clause_alpha_or_beta)
                    varETS_until_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                    clause_until_prime_or_beta = [-varETS_id, -varST_id, -varAlpha_id, -varBeta_id, varETS_beta_id, varETS_until_prime_id]
                    clauses.append(clause_until_prime_or_beta)
                
                elif timestep+1 == len_example:
                    # force beta
                    clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    clauses.append(clause_beta)
            
        return clauses
        
    def get_clauses_release(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.release
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
            # for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size): 
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                
                if timestep+1 < len_example:
                    # either ((aRb)' and b) or (a and b) \equiv b and ( a or (aRb)' )
                    clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    clauses.append(clause_beta)
                    
                    varETS_release_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                    clause_release_prime_or_alpha = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id, varETS_release_prime_id]
                    clauses.append(clause_release_prime_or_alpha)
                
                elif timestep == len_example-1:
                    # force (b)
                    clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    clauses.append(clause_beta)
                    # # force (a and b)
                    # clause_alpha = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
                    # clauses.append(clause_alpha)
  
                    # clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    # clauses.append(clause_beta)
            
        return clauses
        
    def get_clauses_weaknext(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.weaknext
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

        if timestep == len_example-1:
            pass
        else:
            for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
                
                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
    
                varETS_alpha_prime_id = self.varsDict.varETS_id(example_id,timestep+1,alpha_skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_prime_id]
                clauses.append(clause)

        return clauses
        
    def get_clauses_eventually(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.eventually
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
            varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
            if timestep == len_example-1:
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
            else:
                varETS_eventually_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id, varETS_eventually_prime_id]
            clauses.append(clause)
        return clauses
    
    def get_clauses_always(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.always
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
            varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
            clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
            clauses.append(clause)
            
            if timestep + 1 < len_example:
                varETS_always_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_always_prime_id]
                clauses.append(clause)
        return clauses
    
    def get_dual_clauses_and(self,example_id,timestep,skeleton_id):
        # clauses = self.get_clauses_or(example_id,timestep,skeleton_id)
        # return clauses
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.land
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
        #     for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, -varBeta_id, varETS_alpha_id, varETS_beta_id]
                clauses.append(clause)

        return clauses

    def get_dual_clauses_or(self,example_id,timestep,skeleton_id):
        # clauses = self.get_clauses_and(example_id,timestep,skeleton_id)
        # return clauses
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.lor
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
        #     for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                clause_alpha = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
                clauses.append(clause_alpha)
                
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                clauses.append(clause_beta)
            
        return clauses

    def get_dual_clauses_next(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        if timestep + 1 < len_example:
            clauses = self.get_clauses_next(example_id,timestep,skeleton_id,len_example)
        return clauses
        
    # def get_dual_clauses_next(self,example_id,timestep,skeleton_id,len_example):
    #     clauses = []
    #     varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
    #     skeleton_type = Skeletons.next
    #     varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

    #     if timestep == len_example-1:
    #         pass
    #     else:
    #         for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
                
    #             varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
    
    #             varETS_alpha_prime_id = self.varsDict.varETS_id(example_id,timestep+1,alpha_skeleton_id)
    #             clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_prime_id]
    #             clauses.append(clause)

    #     return clauses

    def get_dual_clauses_until(self,example_id,timestep,skeleton_id,len_example):
        # clauses = self.get_clauses_release(example_id,timestep,skeleton_id,len_example)
        # return clauses
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.until
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
            # for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size): 
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                
                if timestep+1 < len_example:
                    # either ((aRb)' and b) or (a and b) \equiv b and ( a or (aRb)' )
                    clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    clauses.append(clause_beta)
                    
                    varETS_release_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                    clause_release_prime_or_alpha = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id, varETS_release_prime_id]
                    clauses.append(clause_release_prime_or_alpha)
                
                elif timestep == len_example-1:
                    # force (b)
                    clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    clauses.append(clause_beta)
                    # # force (a and b)
                    # clause_alpha = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
                    # clauses.append(clause_alpha)
  
                    # clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    # clauses.append(clause_beta)
            
        return clauses

    def get_dual_clauses_release(self,example_id,timestep,skeleton_id,len_example):
        # clauses = self.get_clauses_until(example_id,timestep,skeleton_id,len_example)
        # return clauses
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.release
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        # for alpha_skeleton_id in range(skeleton_id+1, self.fml_size-1):
            # for beta_skeleton_id in range(alpha_skeleton_id+1, self.fml_size):
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            for beta_skeleton_id in range(skeleton_id+2, self.fml_size):

                varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
                varBeta_id = self.varsDict.varBeta_id(skeleton_id,beta_skeleton_id)
    
                varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
                varETS_beta_id = self.varsDict.varETS_id(example_id,timestep,beta_skeleton_id)
                
                if timestep+1 < len_example:
                    # either ((aUb)' and a) or b \equiv ((aUb)' or b) and (a or b)
                    clause_alpha_or_beta = [-varETS_id, -varST_id, -varAlpha_id, -varBeta_id, varETS_alpha_id, varETS_beta_id]
                    clauses.append(clause_alpha_or_beta)
                    varETS_until_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                    clause_until_prime_or_beta = [-varETS_id, -varST_id, -varAlpha_id, -varBeta_id, varETS_beta_id, varETS_until_prime_id]
                    clauses.append(clause_until_prime_or_beta)
                
                elif timestep+1 == len_example:
                    # force beta
                    clause_beta = [-varETS_id, -varST_id, -varBeta_id, varETS_beta_id]
                    clauses.append(clause_beta)
            
        return clauses
    
    def get_dual_clauses_weaknext(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.weaknext
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            
            varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)

            if timestep == len_example-1:
                clause = [-varETS_id, -varST_id, -varAlpha_id]
                clauses.append(clause)
            else:
                varETS_alpha_prime_id = self.varsDict.varETS_id(example_id,timestep+1,alpha_skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_prime_id]
                clauses.append(clause)

        return clauses
        
    def get_dual_clauses_eventually(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.eventually
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
            varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
            clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
            clauses.append(clause)
            
            if timestep +1 < len_example:
                varETS_eventually_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_eventually_prime_id]
                clauses.append(clause)
        return clauses
    
    def get_dual_clauses_always(self,example_id,timestep,skeleton_id,len_example):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.always
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)
        
        for alpha_skeleton_id in range(skeleton_id+1, self.fml_size):
            varAlpha_id = self.varsDict.varAlpha_id(skeleton_id,alpha_skeleton_id)
            varETS_alpha_id = self.varsDict.varETS_id(example_id,timestep,alpha_skeleton_id)
            if timestep == len_example-1:
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id]
            else:
                varETS_always_prime_id = self.varsDict.varETS_id(example_id,timestep+1,skeleton_id)
                clause = [-varETS_id, -varST_id, -varAlpha_id, varETS_alpha_id, varETS_always_prime_id]
            clauses.append(clause)
        return clauses


    # assuming concrete examples
    def get_dual_clauses_literal(self,example_id,timestep,skeleton_id,observation):
        clauses = []
        varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
        skeleton_type = Skeletons.literal
        varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

        for p in range(len(self.vocabulary)):
            literal = p+1
            varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
            if self.vocabulary[p] in observation:
                # this cannot happen
                clause = [-varETS_id, -varST_id, -varLit_id]
                clauses.append(clause)
            
            literal = -(p+1)
            varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
            if self.vocabulary[p] not in observation:
                # this cannot happen
                clause = [-varETS_id, -varST_id, -varLit_id]
                clauses.append(clause)
        
        return clauses
        
                
    # def get_dual_clauses_literal(self,example_id,timestep,skeleton_id,observation):
    #     clauses = []
    #     varETS_id = self.varsDict.varETS_id(example_id,timestep,skeleton_id)
    #     skeleton_type = Skeletons.literal
    #     varST_id = self.varsDict.varST_id(skeleton_id,skeleton_type)

    #     for p in range(len(self.vocabulary)):
    #         # literal (p+1) has not to hold true in the negative example
    #         literal = p+1
    #         varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
            
    #         if self.vocabulary[p] in observation:
    #         # if neg_predicate(self.vocabulary[p]) in observation:
    #             # clause = [-varETS_id, -varST_id, -varLit_id, "False"]
    #             clause = [-varETS_id, -varST_id, -varLit_id]
    #             clauses.append(clause)

    #         # literal -(p+1) has not to hold true in the negative example
    #         literal = -(p+1)
    #         varLit_id = self.varsDict.varLit_id(skeleton_id,literal)
    #         if neg_predicate(self.vocabulary[p]) in observation:
    #         # if self.vocabulary[p] in observation:
    #             # clause = [-varETS_id, -varST_id, -varLit_id, "False"]
    #             clause = [-varETS_id, -varST_id, -varLit_id]
    #             clauses.append(clause)
        
    #     if ABSTRACT_EXAMPLES:
    #         # Necessary when using abstractions in the input examples
    #         # in order to avoid pathological issues with formulas X(p | !p) and
    #         # negative examples [[a],[!a]] for target a & Xa
    #         # IDEA: set X variable in observation!
    #         # if var(e,t,s) var(s,lit) varLit(s',p) then varEx(e,t,p) and !varEx(e,t,!p)
    #         for p in range(len(self.vocabulary)):
    #             pred = p+1
    #             varPred_id = self.varsDict.varLit_id(skeleton_id,pred)
    #             varEx_id = self.varsDict.varEx_id(example_id,timestep,pred)
    #             clause = [-varETS_id, -varST_id, -varPred_id,varEx_id]
    #             # print(clause)
    #             clauses.append(clause)
    
    #             neg_pred = -(p+1)
    #             varNegPred_id = self.varsDict.varLit_id(skeleton_id,neg_pred)
    #             clause = [-varETS_id, -varST_id, -varNegPred_id,-varEx_id]
    #             # print(clause)
    #             clauses.append(clause)
        
    #     return clauses
