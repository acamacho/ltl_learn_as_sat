#!/usr/bin/python3
import enum 
import random
import pycosat  

from modules.cnf import CNF
        
class Stats():
    def __init__(self):
        self.nposex_calls = {}
        self.nnegex_calls = {}
        self.fml = None
        self.time = None
    def inc_posex_calls(self,fml_len):
        if fml_len not in self.nposex_calls:
            self.nposex_calls[fml_len] = 0
        self.nposex_calls[fml_len] += 1
    def inc_negex_calls(self,fml_len):
        if fml_len not in self.nnegex_calls:
            self.nnegex_calls[fml_len] = 0
        self.nnegex_calls[fml_len] += 1
    def n_calls(self):
        nposex_calls = sum(self.nposex_calls[i] for i in self.nposex_calls)
        nnegex_calls = sum(self.nnegex_calls[i] for i in self.nnegex_calls)
        return nposex_calls + nnegex_calls
    def set_found_fml(self,fml):
        self.fml = fml
    # def fml_len(self):
    #     maxlen = 0
    #     for i in [k for k in self.nposex_calls] + [k for k in self.nnegex_calls]:
    #         if i > maxlen:
    #             maxlen = i
    #     return maxlen
    def set_time(self,time):
        self.time = time
    def set_fml_size(self,fml_size):
        self.fml_size = fml_size
        
class PassiveLearner():
    def __init__(self, vocabulary, posexamples, negexamples, record_stats=True):
        self.vocabulary = vocabulary
        self.stats = Stats()
        self.record_stats = record_stats
        
        self.CNF = None
        
        if self.record_stats:
            import time
            self.start_time = time.time()
            self.stats = Stats()
        
        self.posexamples = posexamples
        self.negexamples = negexamples
        
    def get_stats(self):
        return self.stats

    def guess_fml(self):
        # print(len(self.CNF.clauses))
        # print(self.CNF.clauses)
        formula = self.CNF.get_model()
        # print(formula)
        # exit()
        return formula
        # return "F a"

    def learn(self):
        # init CNF with bounded formula size (i.e. number of skeletons)
        def create_cnf(self,fml_size):
            self.CNF = CNF(fml_size,self.vocabulary)
            # we can try different ways to shuffle examples.
            # for now, we do it sequentially and let the SAT solver decide
            # on the order in which satisfy the clauses
            for example in self.posexamples:
                self.CNF.add_pos_example(example)
            for example in self.negexamples:
                self.CNF.add_neg_example(example)
            
        fml_size = 1
        while(fml_size < 14 ):
            fml_size += 1
            print("Searching formula of size %s" % fml_size)
            create_cnf(self,fml_size)
            # self.CNF = CNF(fml_size,self.vocabulary)
            
            guessed_fml = self.guess_fml()
            # guessed_fml = "(%s)" % self.vocabulary[0]
            
            if guessed_fml != None:
                print("SUCCESS. Found formula %s" % guessed_fml)
                if self.record_stats:
                        import time
                        self.stats.set_found_fml(guessed_fml)
                        self.stats.set_time(time.time() - self.start_time)
                        self.stats.set_fml_size(fml_size)
                return guessed_fml
        
        if self.record_stats:
            self.stats.set_found_fml(">14")
            
        print("FAILURE")
        
if __name__ == "__main__":
    vocabulary = ["a","b","c","d"]
    pos_examples = []
    pos_example = [ ["a","b"], ["a","b"] ]
    pos_examples.append(pos_example)
    pos_example = [ ["a","b"], ["a","c"] ]
    pos_examples.append(pos_example)
    pos_example = [ ["a","b"], ["a","c","d"] ]
    pos_examples.append(pos_example)
    
    neg_examples = []
    neg_example = [ ["a","b"], ["a","d"] ]
    neg_examples.append(neg_example)
    neg_example = [ ["a","b"], ["a","b","d"] ]
    neg_examples.append(neg_example)
    
    learner = PassiveLearner(vocabulary,pos_examples,neg_examples)
    learner.learn()
    