#!/usr/bin/python3

# Objective: Learn LTL formula in the form \varphi, where \varphi is an LTLf formula.
from passive_learner import PassiveLearner
from random import shuffle
import os
import numpy as np

    
def rename_predicates(examples):
    # returns vars_dict, a mapping from pred_ids to variable names
    vocabulary = set([])
    for example in examples:
        for state in example:
            vocabulary = vocabulary.union(set(state))

    vocabulary = list(vocabulary)
    vars_dict = {}
    for i in range(len(vocabulary)):
        vars_dict[vocabulary[i]] = "__p%s__" % i
    
    for example in examples:
        for state in example:
            for i in range(len(state)): # for each predicate
                state[i] = vars_dict[state[i]]
    
    
    vars_dict_reversed = {}
    for predicate in vars_dict.keys():
        pred_id = vars_dict[predicate]
        vars_dict_reversed[pred_id] = predicate

    return vars_dict_reversed
    
    
def test_classification(pos_examples_train, pos_examples_test, neg_examples_train, neg_examples_test):
    
    ##################################
    ######### Load Datasets ##########
    ##################################
    pos_examples_train = np.load(pos_examples_train)
    pos_examples_test = np.load(pos_examples_test)
    neg_examples_train = np.load(neg_examples_train)
    neg_examples_test = np.load(neg_examples_test)
    
    
    ##################################
    ####### Rename Predicates ########
    ##################################

    # rename predicates
    # returns vars_dict, a mapping from pred_ids to variable names)
    vars_dict = rename_predicates(pos_examples_train.tolist() + pos_examples_test.tolist() + neg_examples_train.tolist() + neg_examples_test.tolist())
    vocabulary = list(vars_dict.keys())

    pos_examples_train = [example_.tolist() for example_ in pos_examples_train]
    pos_examples_test = [example_.tolist() for example_ in pos_examples_test]
    neg_examples_train = [example_.tolist() for example_ in neg_examples_train]
    neg_examples_test = [example_.tolist() for example_ in neg_examples_test]
    ##################################
    ########## Train Learner #########
    ##################################
    learner = PassiveLearner(vocabulary,pos_examples_train,neg_examples_train)
    learner.learn()

    stats = learner.get_stats()
    formula = stats.fml
    size = stats.fml_size
    runtime = stats.time
    
    # recover the formula with original vocabulary
    # snippet taken from
    # https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string/6117042
    import re
    # use these three lines to do the replacement
    rep = dict((re.escape(k), vars_dict[k]) for k in vars_dict)
    pattern = re.compile("|".join(rep.keys()))
    recovered_formula = pattern.sub(lambda m: rep[re.escape(m.group(0))], formula)
    

    ##################################
    ########## Test Learner #########
    ##################################
    from modules.ltlf_monitor import monitor
    
    n_false_negatives = 0
    for example in pos_examples_test:
        is_ok = monitor(formula,example)
        # print(is_ok)
        if not is_ok:
            n_false_negatives += 1

    n_false_positives = 0
    for example in neg_examples_test:
        is_ok = not monitor(formula,example)
        # print(is_ok)
        if not is_ok:
            n_false_positives += 1

    n_true_positives = len(pos_examples_test) - n_false_negatives
    n_true_negatives = len(neg_examples_test) - n_false_positives

    print("Summary of the Tests:")
    print("False negatives: %s/%s" % (n_false_negatives,len(pos_examples_test)))
    print("False positives: %s/%s" % (n_false_positives,len(neg_examples_test)))
    if (len(pos_examples_test) + len(neg_examples_test)) == 0:
        accuracy = 0
    else:
        accuracy = (n_true_positives + n_true_negatives) / (len(pos_examples_test) + len(neg_examples_test))
    
    if (n_true_positives + n_false_positives) == 0:
        precision = 0
    else:
        precision = n_true_positives / (n_true_positives + n_false_positives)
    if len(pos_examples_test) == 0:
        recall = 0
    else:
        recall = n_true_positives / len(pos_examples_test)
    
    return recovered_formula,size,runtime,accuracy,precision,recall
    
    
if __name__ == "__main__":
    
    import argparse
    
    parser = argparse.ArgumentParser(description='Learns an LTLf formula for ntasks per agent and k examples per task.')
    parser.add_argument('--ntasks', action='store', type=int, help='number of tasks per agent')
    parser.add_argument('--k', action='store', type=int, help='number of examples per task')
    parser.add_argument('--seed', action='store', type=int, help='seed')
    parser.add_argument('--tmpdatadirprefix', action='store', type=str, help='Prefix of the temporal dir created to store sampled examples.')
    args = parser.parse_args()
    
    ntasks = args.ntasks
    k = args.k
    seed = args.seed
    
    import random
    random.seed(seed)
    
    # create temporal dir to store sampled examples
    time_datasets_dir = args.tmpdatadirprefix
    time_ntasks_k_output = os.path.join(time_datasets_dir,"n%s_k%s" % (ntasks,k) )
    os.makedirs(time_ntasks_k_output, exist_ok=True)

    import generate_datasets
    generate_datasets.generate_sampled_dataset(ntasks,k,seed,time_ntasks_k_output)

    pos_examples_train = os.path.join(time_ntasks_k_output,'pos_train.npy')
    pos_examples_test = os.path.join(time_ntasks_k_output,'pos_test.npy')
    neg_examples_train = os.path.join(time_ntasks_k_output,'neg_train.npy')
    neg_examples_test = os.path.join(time_ntasks_k_output,'neg_test.npy')
    
    formula,size,runtime,accuracy,precision,recall = test_classification(pos_examples_train, pos_examples_test, neg_examples_train, neg_examples_test)
    
    import shutil
    # remove temporal dir to store sampled examples
    shutil.rmtree(time_ntasks_k_output)

    print("Epoch: %s" % seed)
    print("Formula: %s" % formula)
    print("Size: %s" % size)
    print("RunTime: %s" % runtime)
    print("NTasks: %s" % ntasks)
    print("K: %s" % k)
    print("Accuracy: %s" % accuracy)
    print("Precision: %s" % precision)
    print("Recall: %s" % recall)
    
