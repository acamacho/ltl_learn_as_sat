import os


# NUM_CORES = 60

# kk = [1,2,3,4,5,6,7,8,9,10,15] # number of examples in the training set
# NTotaltasks = 5



# def run_experiments_python2(dataset_dir,results_dir="results"):
#     from krrt.utils import run_experiment,get_value,append_file

#     # Run your program with different parameters, command-line arguments, etc
#     results = run_experiment(
#         # base_directory = os.path.dirname(os.path.realpath(__file__)), #'/path/to/command/',
#         base_command = 'python3 experiments/openstacks/main_classification.py --input %s ' % dataset_dir,
#         parameters = {
#             '--ntasks': range(1, NTotaltasks+1),
#             '--k': kk
#           },
#         time_limit = TIME_LIMIT,
#         memory_limit = MEM_LIMIT,
#         results_dir = results_dir,
#         progress_file = None, # Print the progress to stdout
#         processors = NUM_CORES # cores
#     )


# will be externally called by subprocess in Python2
def run_experiments_in_time_python2(time,time_datasets_dir,time_results_dir):
    from krrt.utils import run_experiment,get_value,append_file
    
    TIME_LIMIT = 1800
    MEM_LIMIT = 8000
    NUM_CORES = 31

    kk = [1,2,3,4,5,6,7,8,9,10] # number of examples in the training set
    NTotaltasks = 3


    COMMAND = "python3 experiments/openstacks/main_classification.py"
    ARGS = " "
    ARGS += "--seed %s " % time
    ARGS += "--tmpdatadirprefix %s " % time_datasets_dir
    
    # Run your program with different parameters, command-line arguments, etc
    results = run_experiment(
        # base_directory = os.path.dirname(os.path.realpath(__file__)), #'/path/to/command/',
        base_command = "%s %s" % (COMMAND, ARGS),
        parameters = {
            '--ntasks': range(1, NTotaltasks+1),
            '--k': kk
          },
        time_limit = TIME_LIMIT,
        memory_limit = MEM_LIMIT,
        results_dir = time_results_dir,
        progress_file = None, # Print the progress to stdout
        processors = NUM_CORES # cores
    )

def run_experiments_in_time(time,time_datasets_dir,time_results_dir):
    import os,sys,subprocess,shutil
    this_script_name = os.path.basename(__file__).replace('.py','')
    this_folder = os.path.dirname(os.path.realpath(__file__))
    
    
    EXPORT_PYTHONPATH = "export PYTHONPATH=$PYTHONPATH:" + this_folder
    PYTHON2_CALL = """python2 -c 'from %s import run_experiments_in_time_python2; run_experiments_in_time_python2("%s","%s","%s")'""" % (this_script_name,time,time_datasets_dir,time_results_dir)
    COMMAND = "%s; %s" % (EXPORT_PYTHONPATH,PYTHON2_CALL)

    output = subprocess.check_output(COMMAND, shell=True)
    
def remove_temp_files():
    import os,sys,subprocess,shutil
    
    output1 = "N\A"
    output2 = "N\A"
    COMMAND1 = "cd /tmp; ls -lrt | grep -c acamacho"
    COMMAND2 = "find /tmp/ -type f -user acamacho -exec rm -f {} \;"
    try:
        output1 = subprocess.check_output(COMMAND1, shell=True)
        f = open('temp_files.txt','w')
        f.write(output1)
        f.close()
        
        output2 = subprocess.check_output(COMMAND2, shell=True)
    except:
        f = open('error.txt','w')
        f.write("error.\n")
        f.write(COMMAND1)
        f.write("Output=%s\n" % (output1))
        f.write(COMMAND2)
        f.write("Output=%s\n" % (output2))
        f.close()
        

# def run_experiments(dataset_dir,results_dir="results"):
#     import os,sys,subprocess,shutil
#     this_script_path = os.path.realpath(__file__)
#     this_script_name = os.path.basename(__file__).replace('.py','')
#     this_folder = os.path.dirname(os.path.realpath(__file__))
    
    
#     EXPORT_PYTHONPATH = "export PYTHONPATH=$PYTHONPATH:" + this_folder
#     PYTHON2_CALL = """python2 -c 'from %s import run_experiments_python2; run_experiments_python2("%s","%s")'""" % (this_script_name,dataset_dir,results_dir)
#     COMMAND = "%s; %s" % (EXPORT_PYTHONPATH,PYTHON2_CALL)

#     output = subprocess.check_output(COMMAND, shell=True)

    
def write_summary_python2(results_dir="results",results_summary="results/results.csv"):
    from krrt.utils import run_experiment,get_value,append_file

    def parse_result(outfile):
        epoch = get_value(outfile, '.*Epoch: (\d+)\n.*', int)
        formula = get_value(outfile, '.*Formula: (.*)\n.*', str)
        size = get_value(outfile, '.*Size: (\d+)\n.*', int)
        runtime = get_value(outfile, '.*RunTime: ([0-9]+\.?[0-9]*)\n.*', float)
        ntasks = get_value(outfile, '.*NTasks: (\d+)\n.*', int)
        k = get_value(outfile, '.*K: (\d+)\n.*', int)
        accuracy = get_value(outfile, '.*Accuracy: ([0-9]+\.?[0-9]*)\n.*', float)
        precision = get_value(outfile, '.*Precision: ([0-9]+\.?[0-9]*)\n.*', float)
        recall = get_value(outfile, '.*Recall: ([0-9]+\.?[0-9]*)\n.*', float)
        
        
        return epoch, ntasks, k, accuracy, precision, recall, runtime, size, formula
    
    import os
    results_csv = ['Epoch\tNTasks\tK\tAccuracy\tPrecision\tRecall\tRunTime\tSize\tFormula']
    
    output_files = [item_path for item_path in os.listdir(results_dir) if not item_path.endswith(".err") and not item_path.endswith(".csv")]
    for item_path in output_files:
        output_file = os.path.join(results_dir, item_path)
        epoch, ntasks, k, accuracy, precision, recall, runtime, size, formula = parse_result(output_file)
        
        results_csv.append("%s\t%d\t%d\t%f\t%f\t%f\t%f\t%d\t%s" %  (epoch, ntasks, k, accuracy, precision, recall, runtime, size, formula))
    
    append_file(results_summary,results_csv)    

def write_summary(results_dir="results",results_summary="results/results.csv"):
    import os,sys,subprocess,shutil
    this_script_path = os.path.realpath(__file__)
    this_script_name = os.path.basename(__file__).replace('.py','')
    this_folder = os.path.dirname(os.path.realpath(__file__))
    
    
    EXPORT_PYTHONPATH = "export PYTHONPATH=$PYTHONPATH:" + this_folder
    PYTHON2_CALL = """python2 -c 'from %s import write_summary_python2; write_summary_python2("%s","%s")'""" % (this_script_name,results_dir,results_summary)
    COMMAND = "%s; %s" % (EXPORT_PYTHONPATH,PYTHON2_CALL)

    output = subprocess.check_output(COMMAND, shell=True)

def plot_results(results_summary,results_dir):
    import pandas as pd
    import numpy as np
    
    import matplotlib
    matplotlib.use('Agg')

    import matplotlib.pyplot as plt
    from  matplotlib.ticker import FuncFormatter
    import matplotlib.ticker as ticker
    import seaborn as sns
    sns.set()
    sns.set_style("darkgrid", {"axes.facecolor": ".9"})
    sns.set_context("talk")
    
    def plot_accuracy():
        plt.figure() #figsize=(16, 6)
        all_df = pd.read_csv(results_summary, sep='\t', dtype={'Epoch':np.int32,'NTasks':np.int32,'K':int,'Accuracy':np.float64,'Precision':float,'Recall':float })

        all_df = all_df[all_df.NTasks == 1]
        all_df = all_df[all_df.Formula != "0.0"]
        # g = sns.lmplot( x="K", y="Accuracy", data=all_df, fit_reg=True, hue='NTasks', legend=False, markers=["o"])
        
        # Use cubehelix to get a custom sequential palette
        # pal = sns.cubehelix_palette(p, rot=-.5, dark=.3)
        
        # Show each distribution with both violins and points
        g = sns.catplot(x="K", y="Accuracy",kind="violin", data=all_df, bw=.15, cut=0)
        
        # g = sns.lmplot( x="K", y="Accuracy", data=all_df, fit_reg=True, hue='NTasks', legend=False, markers=["o", "x", "1"])
        
        # sns.set(rc={'figure.figsize':(16,9)})

        g.fig.set_size_inches(5,5.5)
        # plt.rcParams['figure.figsize']=(16,9)

        
        # plt.legend(loc='lower right',title="# tasks")
        plt.ylim(bottom=0)
        # plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, _: int(x)))
        # plt.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))
        # plt.gca().xaxis.set_major_formatter(ticker.ScalarFormatter())
        # plt.gca().yaxis.set_major_locator(ticker.MultipleLocator(0.5))
        plt.xlim(left=-0.5,right=9.5)
        plt.gca().set_ylabel('') 
        plt.tight_layout()
        plt.savefig("accuracy.pdf")

    def plot_precision():
        plt.figure()
        all_df = pd.read_csv(results_summary, sep='\t', dtype={'Epoch':np.int32,'NTasks':np.int32,'K':int,'Accuracy':np.float64,'Precision':float,'Recall':float })

        all_df = all_df[all_df.NTasks == 1]
        all_df = all_df[all_df.Formula != "0.0"]
        # g = sns.lmplot( x="K", y="Precision", data=all_df, fit_reg=True, hue='NTasks', legend=False, markers=["o"])
        # g = sns.lmplot( x="K", y="Precision", data=all_df, fit_reg=True, hue='NTasks', legend=False, markers=["o", "x", "1"])
        g = sns.catplot(x="K", y="Precision",kind="violin", data=all_df, cut=0)

        g.fig.set_size_inches(5,5.5)

        # plt.legend(loc='lower right',title="# tasks")
        plt.ylim(bottom=0)
        # plt.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))
        plt.xlim(left=-0.5,right=9.5)
        plt.gca().set_ylabel('') 
        plt.tight_layout()
        plt.savefig("precision.pdf")

    def plot_recall():
        plt.figure()
        all_df = pd.read_csv(results_summary, sep='\t', dtype={'Epoch':np.int32,'NTasks':np.int32,'K':int,'Accuracy':np.float64,'Precision':float,'Recall':float })
        
        all_df = all_df[all_df.NTasks == 1]
        all_df = all_df[all_df.Formula != "0.0"]
        # g = sns.lmplot( x="K", y="Recall", data=all_df, fit_reg=True, hue='NTasks', legend=False, markers=["o"])
        # g = sns.lmplot( x="K", y="Recall", data=all_df, fit_reg=True, hue='NTasks', legend=False, markers=["o", "x", "1"])
        g = sns.catplot(x="K", y="Recall",kind="violin", data=all_df, bw=.15, cut=0)

        g.fig.set_size_inches(5,5.5)
        
        # plt.legend(loc='lower right',title="# tasks")
        plt.ylim(bottom=0)
        # plt.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))
        plt.xlim(left=-0.5,right=9.5)
        plt.gca().set_ylabel('') 
        plt.tight_layout()
        plt.savefig("recall.pdf")
        
    plot_accuracy()
    plot_precision()
    plot_recall()
        
if __name__ == "__main__":
    
    import generate_datasets
    import shutil
    
    results_dir = "results"
    results_summary = os.path.join(results_dir,"results.csv")
    
    # generate_datasets.generate_plans_dataset() #this has to be done one time
    
    NTIMES = 100
    for time in range(NTIMES):
        seed = time
        time_datasets_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),"sampled_data/time_%s" % (time) )
        time_results_dir = os.path.join(results_dir,"time_%s" % time)
        os.makedirs(time_results_dir, exist_ok=True)
        run_experiments_in_time(time,time_datasets_dir,time_results_dir)
        
        # time_datasets_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),"sampled_data/time_%s" % (time) )
        # for ntasks in range(1,NTotaltasks+1):
        #     for k in kk:
        #         time_ntasks_k_output = os.path.join(time_datasets_dir,"n%s_k%s" % (ntasks,k) )
        #         os.makedirs(time_ntasks_k_output, exist_ok=True)
        #         generate_datasets.generate_sampled_dataset(ntasks,k,seed,time_ntasks_k_output)

        # run_experiments(time_datasets_dir,time_results_dir)
        write_summary(time_results_dir,results_summary)

        # remove directory
        shutil.rmtree(time_datasets_dir)
        # remove temp files in /tmp, if any
        remove_temp_files()
    
    f = open(results_summary,'r')
    text = f.read().replace(')Epoch\tNTasks\tK\tAccuracy\tPrecision\tRecall\tRunTime\tSize\tFormula','')
    f.close()
    f = open(results_summary,'w')
    f.write(text)
    f.close()
    
    plot_results(results_summary,results_dir)