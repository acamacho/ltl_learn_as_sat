USAGE
=====

Active Learning
----------------
Active Learning is implemented through an Oracle and active learner agent that performs equivalence queries.
Active learning can be tested by running the command:

    python3 active_learner.py
    
Passive Learning
----------------
Passive Learning is perfomed with respect to a given set of positive and negative examples. 
Passive Learning can be tested by running the command:

    python3 passive_learner.py

Task Classification
--------------------
Given a set of labeled plans of different nature (e.g. different agents performing the same or different tasks in a different manner),
the objective is to learn an LTLf formula that discriminates between a target behavior and the others.
Task Classification can be tested by running the command:

    python3 experiments/openstacks/main_classification.py 

Plan Recognition
-----------------
Given a set of labeled plans of different nature (e.g. different agents performing the same or different tasks in a different manner),
the goal is to learn an LTLf formula that discriminates between a target behavior and the others.


In a second stage, we learn different LTLf formulae to recognize all the individual behaviors. 
Then, we use the learned models for plan recognition. More precisely, the LTLf formulae monitor
executions until only the end of execution, or until only one LTLf formula is consistent with the observed behavior.

