#!/usr/bin/python3

# Objective: Learn LTL formula in the form \varphi, where \varphi is an LTLf formula.

from oracle import Oracle
from active_learner import ActiveLearner


def run_patterns(patterns,output):
    
    with open(output, "w") as output_file:
        output_file.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % ("FmlID","NVars","FoundLen","NExamples","Time","TargetDFASize","Target","Found"))
    
    for pattern_id in range(len(patterns)):
        fml = patterns[pattern_id]
        fml = fml.to_str(parenth=True) # parenthesizing for better compatibility
        vocabulary = list(spot.atomic_prop_collect(fml))
        print("LEARNING PATTERN %s" % fml)
        oracle = Oracle(fml)
        dfa_size = oracle.target_dfa.num_states()
        learner = ActiveLearner(vocabulary,oracle,True,True) # with experience replay and saving stats
        learner.learn()
        stats = learner.get_stats()
        with open(output, "a") as output_file:
            output_file.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (pattern_id,len(vocabulary),stats.fml_len(),stats.n_calls(),stats.time,dfa_size,fml,stats.fml))
 
def plot_results(results_summaries,results_dir):
    import pandas as pd
    import numpy as np
    
    import matplotlib
    matplotlib.use('Agg')

    import matplotlib.pyplot as plt
    from  matplotlib.ticker import FuncFormatter
    import matplotlib.ticker as ticker
    import seaborn as sns
    sns.set()
    sns.set_style("darkgrid", {"axes.facecolor": ".9"})
    sns.set_context("talk")
    
    def plot_runtime(results_summaries):
        # plt.figure() #figsize=(16, 6)
        # Set up the matplotlib figure
        f, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 5), sharex=True)


        # all_df = pd.read_csv(results_summary, sep='\t', dtype={'NVars':np.int32,'FoundLen':np.int32,'K':int,'Time':np.float64,'TargetDFASize':np.int32})
        all_df = pd.concat([pd.read_csv(results_summary, sep='\t', dtype={'NVars':np.int32,'FoundLen':np.int32,'K':int,'Time':np.float64,'TargetDFASize':np.int32}) for results_summary in results_summaries], ignore_index=True)
        
        # all_df = all_df[all_df.NTasks == 1]
        all_df = all_df[all_df.FoundLen > 0]
        # Show each distribution with both violins and points
        # g = sns.catplot(x="FoundLen", y="Time",kind="violin", data=all_df, bw=.15, cut=0,inner="points")
        # g = sns.catplot(x="FoundLen", y="Time", data=all_df)
        # g = sns.catplot(x="FoundLen", y="Time",kind="box", data=all_df)
        g = sns.boxplot(x="FoundLen", y="Time",data=all_df, ax=ax1, color="c")
        g = sns.swarmplot(x="FoundLen", y="Time",data=all_df, linewidth=0,size=3,color=".3", ax=ax1)

        g.set(yscale="log");
        ax1.set_ylabel("Run Time (s)")
        ax1.set_xlabel("")
        
        g = sns.boxplot(x="FoundLen", y="NExamples",data=all_df, ax=ax2, color="c")
        g = sns.swarmplot(x="FoundLen", y="NExamples",data=all_df, linewidth=0,size=3,color=".3", ax=ax2)
        ax2.set_ylabel("# Examples")

        
        # g.fig.set_size_inches(5,5.5)

        plt.xlabel("Formula Size")
        plt.ylim(bottom=0)
        plt.xlim(left=-0.5,right=9.5)
        # plt.gca().set_ylabel('') 
        plt.tight_layout()
        sns.despine(trim=True, left=True, bottom=True)
        plt.subplots_adjust(top=0.99)
        # plt.subplots_adjust()
        plt.savefig("runtime.3.pdf")

    # all_df = pd.concat([pd.read_csv(results_summary, sep='\t', dtype={'NVars':np.int32,'FoundLen':np.int32,'K':int,'Time':np.float64,'TargetDFASize':np.int32}) for results_summary in results_summaries], ignore_index=True)
        
    plt.figure() #figsize=(16, 6)
    plot_runtime(results_summaries)
    
    


if __name__ == "__main__":
    
    import spot
    import spot.gen as sg
    spot.setup()
    
    import os
    results_dir = "results"
    results_summaries = []
    results_summaries.append(os.path.join(results_dir,"active_learner_p_patterns.tsv"))
    results_summaries.append(os.path.join(results_dir,"active_learner_u_patterns.tsv"))
    results_summaries.append(os.path.join(results_dir,"active_learner_r_patterns.tsv"))
    results_summaries.append(os.path.join(results_dir,"active_learner_and_f_patterns.tsv"))
    results_summaries.append(os.path.join(results_dir,"active_learner_dac_patterns.tsv"))
    results_summaries.append(os.path.join(results_dir,"active_learner_nvars3_random.tsv"))
    
    plot_results(results_summaries,results_dir)
    exit()
    
    
    pairs = []
    
    # Pelánek [Spin’07] patterns from BEEM
    LTL_P_PATTERNS = [sg.ltl_pattern(sg.LTL_P_PATTERNS, pattern_id) for pattern_id in range(1,21)]
    pairs.append( (LTL_P_PATTERNS,"active_learner_p_patterns.tsv") )
    # Holeček et al. patterns from the Liberouter project 
    LTL_HKRSS_PATTERNS = [sg.ltl_pattern(sg.LTL_HKRSS_PATTERNS, pattern_id) for pattern_id in range(1,56)]
    pairs.append( (LTL_HKRSS_PATTERNS,"active_learner_hkrss_patterns.tsv") )
    # Somenzi and Bloem [CAV’00] patterns
    LTL_SB_PATTERNS = [sg.ltl_pattern(sg.LTL_SB_PATTERNS, pattern_id) for pattern_id in range(1,28)]
    pairs.append( (LTL_SB_PATTERNS,"active_learner_sb_patterns.tsv") )
    # (p1 U (p2 U (... U pn)))
    LTL_U_RIGHT = [sg.ltl_pattern(sg.LTL_U_RIGHT, pattern_id) for pattern_id in range(1,5)]
    pairs.append( (LTL_U_RIGHT,"active_learner_u_patterns.tsv") )
    # (p1 R (p2 R (... R pn)))
    LTL_R_RIGHT = [sg.ltl_pattern(sg.LTL_R_RIGHT, pattern_id) for pattern_id in range(1,5)]
    pairs.append( (LTL_R_RIGHT,"active_learner_r_patterns.tsv") )
    # Fp1 & Fp2 & Fp3
    LTL_AND_F = [sg.ltl_pattern(sg.LTL_AND_F, pattern_id) for pattern_id in range(1,5)]
    pairs.append( (LTL_AND_F,"active_learner_and_f_patterns.tsv") )
    # FGp1 | FGp2 | FGp3
    LTL_OR_FG = [sg.ltl_pattern(sg.LTL_OR_F, pattern_id) for pattern_id in range(1,5)]
    pairs.append( (LTL_OR_FG,"active_learner_or_fg_patterns.tsv") )
    LTL_DAC_PATTERNS = [sg.ltl_pattern(sg.LTL_DAC_PATTERNS, pattern_id) for pattern_id in range(1,56)]
    # Dwyer et al. [FMSP’98] Spec. Patterns for LTL
    pairs.append( (LTL_DAC_PATTERNS,"active_learner_dac_patterns.tsv") )


    
    num_fml = 200
    for num_variables in range(3,3+1):
        f = spot.randltl(num_variables, tree_size=6, seed=11)
        n_fml = 0
        patterns = []
        while(len(patterns) < num_fml):
            fml = next(f).relabel(spot.Abc)
            fml = spot.unabbreviate(fml,"MW^")
            if "1" not in str(fml) and "0" not in str(fml):
                patterns.append(fml)
        
        output = "active_learner_nvars%s_random.tsv" % (num_variables)
        run_patterns(patterns,output)
            
    for patterns,output in pairs:
        run_patterns(patterns,output)
