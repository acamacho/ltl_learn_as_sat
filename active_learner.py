#!/usr/bin/python3
from oracle import Oracle
import enum 
import random
import pycosat  

from modules.cnf import CNF
        
class Stats():
    def __init__(self):
        self.nposex_calls = {}
        self.nnegex_calls = {}
        self.fml = None
        self.time = None
    def inc_posex_calls(self,fml_len):
        if fml_len not in self.nposex_calls:
            self.nposex_calls[fml_len] = 0
        self.nposex_calls[fml_len] += 1
    def inc_negex_calls(self,fml_len):
        if fml_len not in self.nnegex_calls:
            self.nnegex_calls[fml_len] = 0
        self.nnegex_calls[fml_len] += 1
    def n_calls(self):
        nposex_calls = sum(self.nposex_calls[i] for i in self.nposex_calls)
        nnegex_calls = sum(self.nnegex_calls[i] for i in self.nnegex_calls)
        return nposex_calls + nnegex_calls
    def set_found_fml(self,fml):
        self.fml = fml
    def fml_len(self):
        maxlen = 0
        for i in [k for k in self.nposex_calls] + [k for k in self.nnegex_calls]:
            if i > maxlen:
                maxlen = i
        return maxlen
    def set_time(self,time):
        self.time = time
        
class ActiveLearner():
    def __init__(self, vocabulary, oracle, experience_replay = True, record_stats=True):
        self.vocabulary = vocabulary
        self.oracle = oracle
        self.stats = Stats()
        self.record_stats = record_stats
        
        self.CNF = None
        
        if self.record_stats:
            import time
            self.start_time = time.time()
            self.stats = Stats()
        
        self.experience_replay = experience_replay
        if experience_replay:
            self.posexamples = []
            self.negexamples = []
        
    def get_stats(self):
        return self.stats

    def guess_fml(self):
        # print(len(self.CNF.clauses))
        # print(self.CNF.clauses)
        formula = self.CNF.get_model()
        # print(formula)
        # exit()
        return formula
        # return "F a"

    def learn(self):
        # init CNF with bounded formula size (i.e. number of skeletons)
        def create_cnf(self,fml_size):
            self.CNF = CNF(fml_size,self.vocabulary)
            if self.experience_replay:
                for example in self.posexamples:
                    self.CNF.add_pos_example(example)
                for example in self.negexamples:
                    self.CNF.add_neg_example(example)
        def add_pos_example(self,example):
            self.CNF.add_pos_example(example)
            # print(self.CNF.clauses)
            if self.experience_replay:
                self.posexamples.append(example)
            if self.record_stats:
                self.stats.inc_posex_calls(fml_size)
        def add_neg_example(self,example):
            self.CNF.add_neg_example(example)
            # print(self.CNF.clauses)
            if self.experience_replay:
                self.negexamples.append(example)
            if self.record_stats:
                self.stats.inc_negex_calls(fml_size)
                
        fml_size = 1
        while(fml_size < 14 ):
            fml_size += 1
            print("Searching formula of size %s" % fml_size)
            create_cnf(self,fml_size)
            # self.CNF = CNF(fml_size,self.vocabulary)
            
            # guessed_fml = self.guess_fml()
            guessed_fml = "(%s)" % self.vocabulary[0]
            while (guessed_fml != None):
                print("... guessing formula %s" % guessed_fml)
                if self.oracle.is_target(guessed_fml):
                    print("SUCCESS. Found formula %s" % guessed_fml)
                    if self.record_stats:
                            import time
                            self.stats.set_found_fml(guessed_fml)
                            self.stats.set_time(time.time() - self.start_time)
                    return guessed_fml
                else:
                    if random.choice([0,1]) == 0:
                        example = self.oracle.get_pos_example(guessed_fml)
                        print("... positive example %s" % str(example))
                        if example == None:
                            continue
                        # self.num_examples += 1
                        add_pos_example(self,example)
                        # self.CNF.add_pos_example(example)
                        # print(self.CNF.clauses)
                        # if self.record_stats:
                        #     self.stats.inc_posex_calls(fml_size)
                    else:
                        example = self.oracle.get_neg_example(guessed_fml)
                        print("... negative example %s" % str(example))
                        if example == None:
                            continue
                        # self.num_examples += 1
                        add_neg_example(self,example)
                        # self.CNF.add_neg_example(example)
                        # print(self.CNF.clauses)
                        # if self.record_stats:
                        #     self.stats.inc_negex_calls(fml_size)
                
                # at this point a valid example has been received
                    
                guessed_fml = self.guess_fml()
        
        if self.record_stats:
            self.stats.set_found_fml(">14")
            
        print("FAILURE")
        
if __name__ == "__main__":
    # vocabulary = ["a","b","c"]
    # oracle = Oracle("a & (X a)")
    # oracle = Oracle("(a) | (b)")
    # oracle = Oracle("(X (b U (a & c)))")
    # oracle = Oracle("(b U ( c))")
    # oracle = Oracle("a &X (b & X ( c))")
    # oracle = Oracle("a & X (b)")
    # oracle = Oracle("(a & b)")
    # oracle = Oracle("G (b)")
    # oracle = Oracle("F a")
    # oracle = Oracle("!X(!a)")
    # oracle = Oracle("!(X(G(a)))") # !(X(G(a))) \equiv \top \lor X(!G(a)) \equiv !X(\top) \lor X(F(!a)
    # oracle = Oracle("!(X(F(a)))")
    # oracle = Oracle("(!b | b | X(F((!a))))")
    # oracle = Oracle("(a) U (b)")
    # oracle = Oracle("G( ((b) & ((c) U (d))))")
    # vocabulary = ["a","b","c","d"]
    # oracle = Oracle("G( ((b) & ((c) U (d))))")
    
    vocabulary = ["a","b","c"]
    # oracle = Oracle("(X a)")
    # oracle = Oracle("a & (X a)")
    # oracle = Oracle("(X (a & b))")
    # oracle = Oracle("(N (a & b))")
    # oracle = Oracle("(a U b)")
    oracle = Oracle("(a R b)")

    learner = ActiveLearner(vocabulary,oracle)
    learner.learn()
    
    nposex_calls = learner.stats.nposex_calls
    nnegex_calls = learner.stats.nnegex_calls
    nposex = sum(nposex_calls[i] for i in nposex_calls)
    nnegex = sum(nnegex_calls[i] for i in nnegex_calls)
    print("Number of positive Examples: %s" % nposex)
    print("Number of negative Examples: %s" % nnegex)
    print("Total number of queries: %s" % learner.stats.n_calls())
    print("Learning run time: %s" % learner.stats.time)
    # 
    #  !X(a) \equiv final or X(!a) \equiv N(!a)
    # !X(a) \equiv N(!a)
    # N(a) \equiv X(a) or final
    # !N(a) \equiv X(!a)
    # final \equiv !X(true) \equiv N(false)