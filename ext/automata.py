import spot
import os
LTL3BA = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ltl3ba-1.1.3/ltl3ba') 

def get_automaton(fml, aut_type, ltl2aut, from_ltlf):
    
    if aut_type == "nfw":
        
        if ltl2aut =="ltl2nfa":
            from ext.ltl2nfa import ltl2hoa
            decompose = False        
            automaton_hoa = ltl2hoa.ltl2hoa(fml,decompose)
        else:
            assert(ltl2aut =="ltl2sa")
            from ext.ltlfkit import ltlf2hoa as ltl2hoa
            # from translator.ltl2sa import ltl2hoa
            automaton_hoa = ltl2hoa.ltl2hoa(fml)
        # print(fml)

        # print(automaton_hoa)
        automaton_spot = spot.automaton(automaton_hoa)

        # print(automaton_spot.to_str('dot'))
        return Automaton(automaton_spot)
    else:
        return Automaton.from_specification(fml,aut_type, ltl2aut, from_ltlf)
        # raise("Automaton format not valid.")

def get_automata(fml, aut_type, ltl2aut, from_ltlf):
    decompose = True
    if aut_type == "nfw":
        assert(ltl2aut =="ltl2nfa")
        from translator.ltl2nfa import ltl2hoa
        # print(fml)
        varFormula,varlist, automata_hoa = ltl2hoa.ltl2hoa(fml,decompose)
        # print(automata_hoa)
        automata_spot = spot.automata(automata_hoa)
        aut = []
        for automaton_spot in automata_spot:
            aut.append( Automaton(automaton_spot) )
            # print(automaton_spot.to_str('dot'))
        return varFormula,varlist,aut
    else:
        raise("Automaton format not valid.")    

class Transition:
    src_id = None
    dst_id = None
    guard = None
    _id = None
    
class Automaton():
    def __init__(self, automaton_spot):
        self.transitions = None
        self.aut = automaton_spot
        
    @classmethod
    def from_specification(self,fml,aut_type, ltl2aut, from_ltlf):
        self.transitions = None

        if aut_type == "nfw" or aut_type == "ufw":
            assert(ltl2aut =="ltl2nfa")
    
            if aut_type == "nfw":
                from translator.ltl2nfa import ltl2hoa
                # print(fml)
                hoa = ltl2hoa.ltl2hoa(fml)
                # print(hoa)
                self.aut = spot.automaton(hoa)
                # print(self.aut.to_str('dot'))
            else:
                assert(aut_type == "ufw")
                raise("Compilation not implemented for automaton type.")
            

        elif aut_type == "ucw" or aut_type == "nbw":
            fml_spot = spot.formula(fml)

            # transform LTL-f formula into an equivalent LTL formula
            if from_ltlf:
                fml_spot = spot.from_ltlf(fml_spot)
            
            if aut_type == "ucw":
                # fml = '!(%s)' % fml # negate formula
                fml_spot = spot.formula_Not(fml_spot)  # negate formula
            
            if ltl2aut == "spot":
                # print(fml)
                self.aut = spot.translate(fml_spot, 'BA', 'deterministic', 'high')#'sbacc'ne
                # self.aut = spot.translate(fml_spot, 'BA', 'deterministic', 'high', 'sbacc')# good one
                # self.aut = spot.translate(fml_spot, 'BA', 'small')
                # self.aut = spot.translate(fml_spot, 'BA', 'high', 'unambiguous')
                # self.aut = spot.translate(fml_spot, 'BA', 'deterministic', 'high')#, 'unambiguous', 'complete')#, 'generic', deterministic')
                # print(LTL3BA)
            else:
                assert(ltl2aut =="ltl3ba")
                # print(fml)
                self.aut = spot.automaton('%s -f "%s" |' % (LTL3BA,fml_spot.to_str('spin', parenth=True)))
        else:
            raise("Automaton format not valid.")
        
        return Automaton(self.aut)
    

    def get_accepting(self):
        Q_Fin = []
        for state in range(0, self.aut.num_states()):
            for transition in self.aut.out(state):
                is_accepting = bool(transition.acc)
                # print(transition.acc)
                if is_accepting:
                    # print("is accepting")
                    # print(transition.cond)
                    # print(transition.src)
                    # print(transition.dst)
                    Q_Fin.append(transition.src)
    
        # print(Q_Fin)
        return set(Q_Fin)
    
    def get_accepting_dest(self):
        Q_Fin = []
        for state in range(0, self.aut.num_states()):
            for transition in self.aut.out(state):
                is_accepting = bool(transition.acc)
                # print(transition.acc)
                if is_accepting:
                    # print("is accepting")
                    # print(transition.cond)
                    # print(transition.src)
                    # print(transition.dst)
                    Q_Fin.append(transition.dst)
    
        # print(Q_Fin)
        return set(Q_Fin)
        
    def get_transitions(self):
        if not self.transitions:
            self.transitions = []
            bdict = self.aut.get_dict()
            idx = 0
            for state in range(0, self.aut.num_states()):
                for transition in self.aut.out(state):
                    src = transition.src
                    dst = transition.dst
                    cnf_lbl = spot.formula(spot.bdd_format_formula(bdict, transition.cond))
                    #cnf_lbl formula is already in DNF
                    # print(" %s   acc sets =%s" % (state,transition.acc))
                    # idx = 0
                    for clause in cnf_lbl.to_str().split(" | "):
                        # print(clause)
                        t = Transition()
                        t.src_id = src
                        t.dst_id = dst
                        t._id = idx
                        idx += 1
                        guard = [ x.replace(" ", "") for x in clause.replace("(","").replace(")","").split("&") if x != "1"]
                        t.guard = guard
                        self.transitions.append(t)
                        # print("%s --%s--> %s" %(src,clause,dst))
        # else
        return self.transitions

    def get_transition(self,idx):
        return self.transitions[idx]
        
    def num_states(self):
        return self.aut.num_states()
    
    def num_transitions(self):
        ntransitions = 0
        for state in range(0, self.aut.num_states()):
            ntransitions += len(self.aut.out(state))
            for transition in self.aut.out(state):
                
                return self.aut.num_states()
    
    def get_init_state_number(self):
        return self.aut.get_init_state_number()
 