#!/usr/bin/python3

from ext.ltlfkit import ltlf2hoa
from ext.automata import get_automaton
import spot
from random import shuffle
from modules.ltlf_realizability import get_example,is_null_example,neg_fml,conj_fml

# def get_qstate_transitions(automaton,qstate):
#         from ext.automata import Transition
    
#         transitions = []
#         bdict = automaton.aut.get_dict()
#         for transition in automaton.aut.out(qstate):
#             src = transition.src
#             dst = transition.dst
#             cnf_lbl = spot.formula(spot.bdd_format_formula(bdict, transition.cond))
#             for clause in cnf_lbl.to_str().split(" | "):
#                 # print(clause)
#                 t = Transition()
#                 t.src_id = src
#                 t.dst_id = dst
#                 guard = [ x.replace(" ", "") for x in clause.replace("(","").replace(")","").split("&") if x != "1"]
#                 t.guard = guard
#                 transitions.append(t)
#                 # print("%s --%s--> %s" %(src,clause,dst))
        
#         return transitions

# def get_example(formula):
#     # return a finite trace that satisfies the input formula
#     automaton = get_automaton(formula, "nfw", "ltl2sa", None)
    
#     # detect UNSAT
#     if automaton.get_init_state_number() == 0 and automaton.num_states() == 1:
#         return None
    
#     # do breath-first search to find an example
    
#     Q_accepting_dest = automaton.get_accepting_dest()
    
    
#     init = automaton.get_init_state_number()
#     visited = []
#     queue = [(init,[])]
#     while(len(queue) > 0):
#         cur_qstate, partial_plan = queue.pop()
#         if cur_qstate in visited:
#             continue
#         visited.append(cur_qstate)
#         transitions = get_qstate_transitions(automaton,cur_qstate)
#         # transitions = shuffle(transitions)
#         # print(cur_qstate)
#         # print(transitions)
#         for transition in transitions:
#             nxt_state = transition.dst_id
#             nxt_partial_plan = partial_plan + [transition.guard]
            
#             if nxt_state in Q_accepting_dest:
#                 # print(str(nxt_partial_plan))
#                 return nxt_partial_plan
                
#             queue.append( (nxt_state,nxt_partial_plan) )
#             # print(nxt_partial_plan)
        
    
#     # return ["a","b","c"]
#     print(Q_accepting_dest)
#     return None

    
# def is_null_example(example):
#     # return True iff example is null
#     # if len(example) == 0:
#     #     print("Example was []")
#     return example == None
#     # return len(example) == 0
    
# def neg_fml(formula):
#     return "!( %s )" % formula
    
# def conj_fml(fml1, fml2):
#     return "( %s ) & ( %s )" % (fml1,fml2)


# Objective: Output a positive or negative example.
# Determine if the given formula is equivalent to the target
class Oracle():
    def __init__(self, target_fml):
        self.target_fml = target_fml
        self.target_dfa = get_automaton(self.target_fml, "nfw", "ltl2sa", None)
    
    def is_target(self, guessed_formula):
        # return True if guessed_formula is equivalent to the target
        pos_example = self.get_pos_example(guessed_formula)
        if not is_null_example(pos_example):
            return False
        neg_example = self.get_neg_example(guessed_formula)
        if not is_null_example(neg_example):
            return False
        # no positive nor negative example exists at this point
        # so the guessed formula must be equivalent to the target
        return True
        
    def get_pos_example(self, guessed_formula):
        # construct a formula \neg guessed_formula \land target_fml
        # call planner and get plan
        # reverse the plan? dualize operators?
        test_fml = conj_fml( neg_fml(guessed_formula), self.target_fml )
        return get_example(test_fml)
        
    def get_neg_example(self, guessed_formula):
        # construct a formula \neg guessed_formula \land target_fml
        # call planner and get plan
        # reverse the plan? dualize operators?
        test_fml = conj_fml( neg_fml(self.target_fml), guessed_formula )
        return get_example(test_fml)


if __name__ == "__main__":
    oracle = Oracle("G p")
    oracle.get_pos_example("X p")
    oracle.get_neg_example("F p")
    